package org.tomahna.scala.tutorial.ex2

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex2.DefinitionsAndEvaluationSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex2.DefinitionsAndEvaluationSpec"

  Sources:
  https://www.scala-exercises.org/scala_tutorial/definitions_and_evaluation
  https://www.scala-exercises.org/scala_tutorial/functional_loops
 */
object DefinitionsAndEvaluation {

  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////// Nommage /////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Pour rendre des expressions complexes plus lisibles, on peut leur donner des noms aux expressions intermédiaires
    Par exemple pour l'expression calculant la surface d'un disque de rayon 10:
    3.14159 * 10 * 10
   */
  val radius = 10
  val pi = 3.14159
  val area: Double = ???

  /*
   Notez que le type de radius, pi et area n'est pas spécifié. Le compilateur scala est capable
   d'inféré le type à partir des informations dont il dispose.

   >>>>>> CEPENDANT, si les informations dont il dispose sont fausse (donc que vous vous êtes trompez), <<<<<
   >>>>>> le type inféré peut être faux. Par conséquent, il est fortement recommandé de spécifier le    <<<<<
   >>>>>> type de retour des valeurs accessible de l'extérieur (de la classe / objet / fonction         <<<<<
   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
   */

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////// Méthodes ///////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    On définit une fonction/méthode avec le keyword def. Le format est le suivant
    def nomMéthode(param1: TypeParam1): TypeRetour = { corps de la fonction }

    Si la fonction ne fait qu'une ligne les {} sont optionnels
   */
  // Par exemple
  def square(x: Double): Double = x * x

  // Définissez la fonction area2 qui calcul l'aire d'un disque en prenant le rayon en paramètre
  // Note: Le test associé est un peu buggé donc si il ne passe pas demandez moi
  def area2 = ???

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////// Paramètres multiples //////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Les paramètres multiples sont séparez par des virgules

  /**
    * Renvoit square(x) + square(y)
    * Note: Le test associé est un peu buggé donc si il ne passe pas demandez moi
    */
  def sumOfSquares = ???


  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////// Def vs Val /////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // les expressions de type def sont évaluées à chaque utilisation
  // les expressions de type val sont évaluées une seule fois


  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////// Expressions conditionnelles //////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /* Les expressions conditionnelles sont exprimé de manière similaire à d'autre language
     def add(x: Int, y: Int): Int =
      if(y == 0){
        x
      } else {
        x + y
      }
  */

  /**
    * Calcul la valeur absolue de x
    */
  def abs(x: Int): Int = ???

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////// Call-By-Name et Call-By-Value (Un peu plus avancé) ///////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    En callByValue, l'argument est évalué une seule fois directement à l'appel de la méthode
   */
  def callByValue(value: String): String = value

  /*
    En callByName, l'argument est évalué à chaque fois qu'il est utilisé (et peut donc ne pas être
    évalué)
    Dans la fonction suivante, n2 ne sera jamais évalué
   */
  def callByName(n1: => String, n2: => String): String = n1

  /*
   Les arguments callByName sont notament utilisés dans les loggers scala pour n'évaluer le message
   que quand il est nécessaire
   */
  val isLogLevelInfo = false
  def infos(message: => String): Unit =
    if (isLogLevelInfo) {
      println(message)
    }
}
