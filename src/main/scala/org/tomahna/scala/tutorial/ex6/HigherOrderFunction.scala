package org.tomahna.scala.tutorial.ex6

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex6.StructuringInformationSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex6.StructuringInformationSpec"

  Sources:
  https://www.scala-exercises.org/scala_tutorial/higher_order_functions
 */
object HigherOrderFunction {
  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////// Higher-Order Functions //////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Dans un language fonctionnel, une fonction est traité comme une valeur de première classe.

    Cela signifie qu'une fonction peut être passer en paramètre et retournée en resultat.

    Cela fournit une façon flexible de composer un programme.

    On appelle une fonction qui prend en paramètre une autre fonction, une fonction d'ordre supérieur
   */

  //Par exemple, prenont les fonctions simple suivante
  def sumInts(a: Int, b: Int): Int =
    if (a > b) 0 else a + sumInts(a + 1, b)

  def cube(x: Int): Int = x * x * x

  def sumCubes(a: Int, b: Int): Int =
    if (a > b) 0 else cube(a) + sumCubes(a + 1, b)

  // sumInts et sumCubes sont très similaires, en programmation fonctionnelle on peut les factoriser
  def sum(f: Int => Int, a: Int, b: Int): Int =
    if (a > b) 0
    else f(a) + sum(f, a + 1, b)

  def id(x: Int): Int = x
  def sumIntsBis(a: Int, b: Int): Int = sum(id, a, b)
  def sumCubesBis(a: Int, b: Int): Int = sum(cube, a, b)

  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////  Function Types ///////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Le type A => B est le type d'une fonction qui prend une valeur de type A en argument et retourne une valeur de type B
  // Int => Int est donc une fonction qui va des entiers vers les entiers

  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////  Anonymous Function Syntax ////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Il peut parfois être fastidieux de définir des fonction simples, on peut utiliser des fonctions anonymes

  (x: Int) => x * x * x //est une fonction anonyme qui met son argument au cube
  (x: Int, y: Int) => x + y // est une fonction anonyme avec plusieurs paramètres

  // On peut donc réecrire les fonction précédentes en utilisant des fonctions anonymes
  def sumInts3(a: Int, b: Int) = sum(x => x, a, b)
  def sumCubes3(a: Int, b: Int) = sum(x => x * x * x, a, b)

  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////  Exercice /////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Exprimez les fonctions suivante en utilisant une unique fonction d'ordre supérieur

  /**
    * If user is Barry return "Hello, Barry!" else return "Meh"
    */
  def greetBarry(name: String): String = ???

  /**
    * If user is Paul return "Hello, Paul!" else return "Meh"
    */
  def greetPaul(name: String): String = ???

}
