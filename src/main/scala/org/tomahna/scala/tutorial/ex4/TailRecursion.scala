package org.tomahna.scala.tutorial.ex4

import scala.annotation.tailrec

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex4.TailRecursionSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex4.TailRecursionSpec"

  Sources:
  https://www.scala-exercises.org/scala_tutorial/tail_recursion
 */
object TailRecursion {
  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////  Recursive Function Application /////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Considérons la fonction récursive suivante qui calcule le pgcd en utilisant l'algorithme d'Euclid
  def gcd(a: Int, b: Int): Int =
    if (b == 0) a else gcd(b, a % b) // a % b veut dire a modulo b

  //gcd(14, 21) est évalué comme suit
  gcd(14, 21)
  if (21 == 0) 14 else gcd(21, 14 % 21)
  if (false) 14 else gcd(21, 14 % 21)
  gcd(21, 14 % 21)
  gcd(21, 14)
  if (14 == 0) 21 else gcd(14, 21 % 14)
  if (false) 21 else gcd(14, 21 % 14)
  gcd(14, 7)
  gcd(7, 14 % 7)
  gcd(7, 0)
  if (0 == 0) 7 else gcd(0, 7 % 0)
  if (true) 7 else gcd(0, 7 % 0)
  7


  // Maintenant considérons factorielle
  def factorial(n: Long): Long =
    if (n == 0L) 1L else n * factorial(n - 1L)

  //factorial(4) est évaluée comme suit
  factorial(4)
  if (4 == 0) 1 else 4 * factorial(4 - 1)
  4 * factorial(3)
  4 * (3 * factorial(2))
  4 * (3 * (2 * factorial(1)))
  4 * (3 * (2 * (1 * factorial(0))))
  4 * (3 * (2 * (1 * 1)))
  24

  /*
   La différence entre les deux est que l'expression de factorielle augmente au fur et à mesure des
   opérations avant qu'on puisse. La JVM limite l'expansion de cette expression à une centaine, après
   elle renvoit une StackOverflowException.
    */

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////  Tail Recursion  /////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Une fonction est récursive terminale lorsqu'elle se termine par un appel à soit même
    Dans le cas de gcd, le dernier appel est gcd(b, a % b) qui est un appel à soit même
    Pour factorial, le dernier appel est n * factorial(n - 1), le dernier appel est donc n * ... plus récursions

    Une fonction récursive terminale peut-être optimisée, une fonction non récursive terminale ne le peut pas

    TLDR: Une fonction récursive DOIT être terminale
   */

  /*
    * L'annotation @tailrec permet de s'assurer que la fonction est récursive terminale
    */

  // Completez la version recursive terminale de la fonction factorial
  def factorialTR(n: Int): Int = {
    @tailrec
    def iter(x: Int, result: Int): Int =
      if (???) result
      else iter(???, result * x)

    iter(n, ???)
  }

}
