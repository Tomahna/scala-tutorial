package org.tomahna.scala.tutorial.ex9

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Sources:
  https://www.scala-exercises.org/std_lib/implicits
 */
object Implicits {
  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////  Implicits  /////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Les implicits sont un sujet très à part dans Scala. Il y a ceux qui adorent et ceux qui détestent.
    En revanche, c'est une des feature les plus emblématique du language Scala (dixit Martin Odersky)

    Prenons l'exemple d'un convertisseur JSON
   */
  /**
    * Une simple interface qui permet de convertir un type T en json
    */
  trait JsonConverter[T] {
    def toJson(t: T): String
  }

  /**
    * Une fonction qui permet de convertir n'importe quel type T en json
    * A CONDITION que le compilateur trouve un JsonConverter pour T
    */
  def toJson[T](t: T)(implicit c: JsonConverter[T]): String =
    c.toJson(t)

  // Jusque la on ne peut rien faire car le compilateur n'a accès à aucun JsonConverter
  // Définissons en un
  implicit val intConverter: JsonConverter[Int] = new JsonConverter[Int] {
    override def toJson(t: Int) = t.toString
  }
  /*
    Ici c'est deux choses, d'une part c'est une classe annonyme d'autre part comme elle n'a qu'une
    méthode c'est ce qu'on appelle une SAM ou Single Abstract Method. Je précise parce que normalement
    IntelliJ se plaint que c'est pas super optimisé ce que j'ai écrit.
   */

  //Yay on peut maintenant convertir des entiers en Json !
  toJson(1) //ça compile donc ça marche \o/, ici le compilateur transforme ça en toJson(1)(intConverter)

  // Définissons en quelques autre
  implicit val stringConverter : JsonConverter[String] = (t: String) => t //C'est la façon courte d'écrire une SAM (et du coup IntelliJ est content \o/)
  implicit val doubleConverter: JsonConverter[Double] = (t: Double) => t.toString

  toJson("hello")
  toJson(4.3) //ça compile encore, yay !

  // Et c'est la que ça devient vraiment intéressant, on peut définir ça sur des types complexes en utilisant les implicits
  /**
    * Ca, ça dit, si j'ai un convertisseur json pour un type T quelconque, alors j'ai un convertisseur pour List[T]
    */
  implicit def listConverter[T](implicit c: JsonConverter[T]): JsonConverter[List[T]] =
    (t: List[T]) => t.map(x => c.toJson(x)).mkString("[", ",", "]")

  toJson(List(1, 2, 3))
  //Le compilateur transforme ça en toJson(List(1, 2, 3))(listConverter(intConverter)), avouez qu'il est bon quand même !

  /**
    * Pareil pour option
    */
  implicit def optionConverter[T](implicit c: JsonConverter[T]): JsonConverter[Option[T]] =
    (t: Option[T]) => t.fold("null")(x => c.toJson(x))

  /*
    Normalement à ce stade il y a deux écoles
    - au mon dieu de la magie c'est génial
    - au bucher hérétique !

    Il y a plusieurs choses à retenir
    - Certains types de base Scala utilisent des implicits (c'est rare et moins compliqué que ça)
    - Par contre il y a plein de librairies vraiment intéressantes qui les utilisent (Circe, Cats, etc...)
    - Ca peut demander un peu de doigté dans le debuggage, donc à réserver à des uses cases sur lesquels ça a de la plus value
    - Au moins ça plante à la compilation, c'est toujours mieux que d'autres language, wink wink javascript
   */

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////  Exercices  /////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Je vous épargne ça (et par la même m'épargne d'avoir à écrire le test)
    Si vous voulez vous pouvez écrire un floatConverter et un eitherConverter
   */
}
