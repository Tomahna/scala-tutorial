package org.tomahna.scala.tutorial.ex5

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex5.StructuringInformationSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex5.StructuringInformationSpec"

  Sources:
  https://www.scala-exercises.org/scala_tutorial/structuring_information
 */
object StructuringInformation {
  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////// Aggregating Information With Case Classes  /////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Supposons que l'on souhaite représenter une partition de musique.
    Une note est composé de trois paramètre, un nom (DO, RE, MI, ...), une durée(noir, blanche, croche, ...) et une octave

    Pour modéliser cela nous pouvons utiliser une case class Scala
   */
  case class Note(name: String, duration: String, octave: Int)

  // Nous venons de définir ce qu'est une note, maintenant nous voulons créer une note réel.
  val c1 = Note("DO", "noir", 3)

  // On peut retrouver les informations dans c1
  c1.name
  c1.duration
  // etc...

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////  Defining Alternatives With Sealed Traits  /////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Cependant un symbol musical peut être soit une note soit un silence.

    On peut représenter cela en utilisant un trait

    Dans le code suivant Note2 et Rest sont tout les deux des Symbol.

    Un trait scellé est un trait dont tout les sous-types sont contenu dans ce fichier, c'est
    important car cela permet au compilateur scala d'avoir plus d'information (et donc d'aider)
   */
  sealed trait Symbol
  case class Note2(name: String, duration: String, octave: Int) extends Symbol
  case class Rest(duration: String) extends Symbol

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////  Pattern Matching   //////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Comme Symbol n'a pas de membres, on ne peut rien faire avec. On n'a donc besoin de faire la
    distinction entre les deux cas de symbol. On peut donc utiliser le pattern matching.
   */
  def symbolDuration(symbol: Symbol): String =
    symbol match {
      case Note2(name, duration, octave) => duration
      case Rest(duration) => duration
    }

  /*
    Le pattern matching précédent vérifie si le Symbol est une Note ou un Rest et extrait les champs.
    Il évalue ensuite l'expression suivant la flèche =>
   */

  /*
    Comme Symbol est un trait scélé le compilateur scala peut s'assurer que le pattern matching est exhaustif.
    Le code suivant génèrera un warning à la compilation
   */
  def nonExhaustif(symbol: Symbol): String =
    symbol match {
      case Rest(duration) => duration
    }

  // Quelques autres exemples de pattern matching
  def isOneOrPair(number: Int): Boolean = number match {
    case 1 => true
    case i if i % 2 == 0 => true
    case _ => false // cas par défaut
  }

  def helloBarry(name: String): String = name match {
    case "Barry" => "Hello Barry"
    case _ => "You're not Barry"
  }

  def doIsBroken(symbol: Symbol): String = symbol match {
    case _: Rest =>
      "Shut" //Ici on ne vérifie que le type, peut importe le contenu de Rest
    case Note2("Do", _, _) =>
      "Cassé" // _ signifie que l'on n'extrait pas le contenu
    case Note2(name, _, _) => name
  }

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////  Enumerations   //////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // On peut également se servir de type scélé pour représenter des énumérations
  sealed trait NoteName
  case object A extends NoteName
  case object B extends NoteName
  case object C extends NoteName

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////   Algebraic Data Types ///////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
   Un type définit à partir de case classes et d'objets étendant un type scélé est appelé Algebraic Data Types (ADT).
   Il s'agit simplement d'un set de valeur possibles. On peut aussi appelé ça un type coproduit #JeRameneMaScience
   */

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Exercice /////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    On représente des profils utilisateur d'un systèmes quelconque.
    Un utilisateur peut être soit Administrateur soit Utilisateur.
    Barry et Paul sont administrateur, le reste du monde est utilisateur.

    Implémenter la gestion de profile via case class et pattern matching
   */
  sealed trait Profile

  def getProfile(name: String): Profile = ???

  def isAdmin(user: Profile): Boolean = ???

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////   Classes ////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Les cases classes sont un type spécial de classe. On peut également écrire des classes simple en scala.

    La différence entre une classe et une case classe est que, pour une case classe, le compilateur génère
    un certains nombre de méthodes supplémentaires qui permettent, entre autre, le pattern matching
   */
  class Point(x: Int, y: Int) {
    override def toString(): String = "(" + x + "," + y + ")"
  }

  // Pour instancier une classe il faut utiliser le mot clé new
  val p1 = new Point(1, 2)

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////   Object /////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Un objet est un singleton, c'est l'équivalent du mot clé static en Java
   */
  object Greeting {
    val english = "Hi"
    val espanol = "Hola"
  }
  Greeting.english
  Greeting.espanol

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////   Companion Object ///////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Lorsque l'on retrouve un object et une classe ayant le même nom dans un même fichier scala on
    appele l'objet un companion object.
    Il peut par exemple servir à définir une factory method pour la classe
   */
  class Movie(name: String, year: Short)

  object Movie {
    def academyAwardBestMoviesForYear(x: Short): Movie = {
      //This is a match statement, more powerful than a Java switch statement!
      x match {
        case 1930 ⇒ new Movie("All Quiet On the Western Front", 1930)
        case 1931 ⇒ new Movie("Cimarron", 1931)
        case 1932 ⇒ new Movie("Grand Hotel", 1932)
        case _ => new Movie("Not Found", 0)
      }
    }
  }

  Movie.academyAwardBestMoviesForYear(1932)
}
