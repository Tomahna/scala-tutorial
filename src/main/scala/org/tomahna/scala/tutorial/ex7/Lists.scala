package org.tomahna.scala.tutorial.ex7

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex7.ListsSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex7.ListsSpec"

  Sources:
  https://www.scala-exercises.org/std_lib/lists
 */
object Lists {
  /*
    Scala dispose d'un système de collection assez riche mais également assez complexe
    http://docs.scala-lang.org/resources/images/collections.immutable.png

    Cela dit, à quelques exceptions près les fonctions disponible sur les différentes collections
    sont similaires.

    Nous allons dans un premier temps nous intéresser aux sous-types de Seq
   */

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// Seq ///////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // On peut instancier des Seq de la manière suivante
  val aList: List[Int] = List(1, 2, 3) // instantiation d'une liste
  val aSeq: Seq[Int] = Seq(1, 2, 3) // instantiation d'une Seq
  val aIndexedSeq: IndexedSeq[Int] = IndexedSeq(1, 2, 3) // instantiation d'une IndexedSeq

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// head, headOption, tail ////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    On peut obtenir le premier élément d'une Seq via la méthode head
    Cela dit si la liste est vide la méthode head renverra une exception, pour éviter cela on peut
    utiliser headOption qui renvoit une Option (cf ex8)

    De même on peut obtenir la queue avec la méthode tail
   */
  val headAList: Int = aList.head
  val headASeq: Int = aSeq.head
  val tailAList: List[Int] = aList.tail

  /*
    On peut obtenir l'élement n via aList(n)
   */
  val elem3AList: Int = aList(1)

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// Immutability///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Scala dispose de collections immutables (et également de collections mutables à éviter autant
    que possible). Cela signifie qu'elles ne peuvent pas être modifier. Par conséquent, toute opération
    sur une collection immutable crée une nouvelle collection.
   */

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// Filter, Map, FlatMap //////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Les collections scala disposent de nombreuses méthodes utiles:
    length -> donne la taille de la collection
    reverse -> inverse l'ordre de la liste
    sort -> tri la liste

    Elles disposent également de ce qu'on appel des fonctions de transformations
    map -> applique une opération à tout les collection de la liste
    flatmap -> applique une opération qui renvoit une collection à tout les élements de la collection et concatène les collections résultats
    filter -> applique un filtre à la liste
   */

  aList.map(x => x * 2) //donne List(2, 4, 6)
  aList.filter(x => x % 2 == 1) //donne List(1, 3)
  aList.flatMap(x => List(x, x, x)) //donne List(1,1,1,2,2,2,3,3,3)

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// Exercice //////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /**
    * Input is a list of CSV formatted lines
    * First split them using the separator , (use String.split)
    * Second filter String whose longer is more than 10 (Use String.length)
    * Then transform them into lowerCases (String.toLowerCases)
    */
  def csvCellLowerCases(s: Seq[String]): Seq[String] = ???

  //////////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////  Fold ////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Les collections disposent également de fonctions d'aggregations

   reduce -> applique une fonction aux éléments 2 à 2
   fold -> applique une fonction aux éléments 2 à 2 avec un seed
   aggregate -> c'est compliqué, et pas descriptible sans un joli schéma
   */

  aList.reduce((x, y) => x * y) // donne ((1 * 2) * 3) = 6 mais renvoit une exception si la liste est vide
  aList.fold(1)((x, y) => x * y) // donne ((1 * 2) * 3) = 6 aussi mais renvoit 1 si la liste est vide

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// Exercice //////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /**
    Input is a list of String
    Return the sum of the string sizes

    Ex: List("abc", "def") return 6
    Hint: Try to use types to guide you, or else just ask !
   */
  def sumSizes(s: Seq[String]): Int = ???

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////// Maps et Sets //////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Globalement c'est pareil et j'ai la flemme donc allez regarder la
    https://www.scala-exercises.org/std_lib/maps
    https://www.scala-exercises.org/std_lib/sets
   */
}
