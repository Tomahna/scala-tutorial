package org.tomahna.scala.tutorial.ex3

import org.tomahna.scala.tutorial.ex2.DefinitionsAndEvaluation.abs

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex3.LexicalScopesSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex3.LexicalScopesSpec"

  Sources:
  https://www.scala-exercises.org/scala_tutorial/lexical_scopes
 */
object LexicalScopes {
  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////// Fonction Nested /////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////

  /*
    Une bonne pratique en programmation fonctionnelle (et non fonctionnelle également d'ailleurs)
    est de divisé une tache en plusieurs petites fonctions

    Le nom et la nature de ces fonctions dépend uniquement de l'implémentation de la fonction. En
    général, on ne veut pas qu'un utilisateur ai accès à ces fonctions.

    Une façon de faire est de mettre les fonctions auxiliaires à l'intérieur de la fonction
   */
  /**
    * Calcul la valeur absolue de x
    */
  def sqrt(x: Double): Double = {
    def sqrtIter(guess: Double, x: Double): Double =
      if (isGoodEnough(guess, x)) guess
      else sqrtIter(improve(guess, x), x)

    def improve(guess: Double, x: Double) =
      (guess + x / guess) / 2

    def isGoodEnough(guess: Double, x: Double) =
      math.abs((guess * guess) - x) < 0.001

    sqrtIter(1.0, x)
  }

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////  Bloques en Scala ///////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Un bloque est délimité par des acolades { ... }
  {
    val x = 3
    x * x
  }
  // Il contient une séquence de définitions et d'expressions
  // Le dernier élément d'un bloque définit sa valeur
  // Les bloques sont des expressions. Bloques et expressions sont interchangeables

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////  Bloques et visibilité //////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Les définitions au sein d'un bloque ne sont visible que depuis l'intérieur du bloque
  // Les définitions au sein d'un bloque surpassent les définitions ayant un même nom en dehors du
  // block (à éviter)
  val x = 0
  def f(y: Int) = y + 1
  val result = {
    val x = f(3)
    x * x
  } + x
  /**
    * Doit être égale à la valeur de result
    */
  val answer: Int = ???

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////  Points Virgules ////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Les points virgules pour marquer la fin d'une ligne sont optionnels
  val a = 1;

  /*
   Attention, les expressions de type:
   someLongExpression
   +someOtherExpression

   seront interprété comme suit:

   someLongExpression;
   +someOtherExpression

   Deux solutions
   - soit mettre des parenthèses:
   (someLongExpression
    + someOtherExpression)

   - Soit écrire l'opérateur sur la première ligne:
   someLongExpression +
     someOtherExpression
   */

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////   Top-Level Definitions /////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Les méthodes et valeurs définis au sein d'un objet son accessible sous la forme Object.method
  // Nous sommes dans l'objet ScopesAndRecursion donc:
  LexicalScopes.sqrt(10)

  //////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////   Packages and Imports  /////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Les objets et les classes peuvent être organiser en package
  // Nous sommes dans l'objet ScopesAndRecursion qui se trouve dans le package org.tomahna.scala.tutorial.ex3
  // cf définition ligne 1 du fichier

  // Toutes les classes/objets définis au sein d'un même package sont accessible directement

  // Les classes/objets définis dans un autre package nécessite soit un import
  // cf ligne 3 du fichier
  abs(1)
  // soit l'utilisation du nom complet
  org.tomahna.scala.tutorial.ex2.DefinitionsAndEvaluation.square(10)

  // Tout les membres des packages scala, java.lang et scala.Predef sont importés par défaut

  /**
    * la valeur de Baz.y
    */
  val bazY = ???
}

object Foo {
  val x = 1
}
object Bar {
  val x = 2
}
object Baz {
  import Bar.x
  val y = x + Foo.x
}