package org.tomahna.scala.tutorial.ex10

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex8.FuturesForComprehensionSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex10.FuturesForComprehensionSpec"
 */
object FuturesForComprehension {
  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Future ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Les futures représente des morceaux de code asynchrone. Typiquement un appel à une base de donnée
   */
  def fetchUserName(id: Int): Future[String] = Future {
    if (id == 1) "Barry" else "Paul"
  }

  def fetchBalance(id: Int): Future[Int] = Future {
    if (id == 1) 100000 else -10000
  }

  /*
    De même que pour les options on dispose des fonctions map, flatMap, filter, etc, etc...
    La différence c'est qu'un appel à une base de donnée peut planter donc il y a des méthodes
    supplémentaires comme recover, recoverWith, etc...
   */

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// For Compréhension ////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Un cas assez classique dans une appli est d'aller chercher consécutivement plusieurs information
    dans une base de donnée. On peut alors faire cela de la façon suivante
   */

  def computeMessage(id: Int): Future[String] =
    fetchUserName(id) //On récupère le userName
      .flatMap { user =>
        fetchBalance(id) //On récupère l'état du compte
          .map { balance =>
            if (balance > 0) {
              s"Bravo $user vous êtes dans le vert" //C'est une interpolation de string (faut juste mettre le s devant)
            } else {
              s"$user vous êtes encore dans le rouge"
            }
          }
      }

  /*
    C'est bien mais il y a deux problèmes
    1) C'est complètement illisible
    2) C'est pas du tout optimal parce qu'on execute les actions séquentiellement

    En pratique, on a pas besoin d'attendre d'avoir le userName pour chercher l'état du compte
   */

  // Commençons par améliorer la lisibilité, il y a un truc qui s'appelle une for compréhension
  def computeMessageForComp(id: Int): Future[String] =
    for {
      user <- fetchUserName(id)
      balance <- fetchBalance(id)
    } yield {
      if (balance > 0) s"Bravo $user vous êtes dans le vert"
      else s"$user vous êtes encore dans le rouge"
    }

  /*
   C'est EXACTEMENT pareil, c'est juste du sucre syntaxique. Et ça marche sur n'importe quel type
   qui définit un map, un flatMap et un filter (donc les options, either, future, list, seq, ...)

   Quand vous avez des fonctions dans des fonctions dans des fonctions en général c'est une bonne
   idée d'utiliser un for compréhension

   Cela dit ça ne règle pas le 2eme problème puisque comme je l'ai dit c'est exactement équivalent
   */

  def computeMessageParallel(id: Int): Future[String] = {
    val eventualUser = fetchUserName(id)
    val eventualBalance = fetchBalance(id)
    for {
      user <- eventualUser
      balance <- eventualBalance
    } yield {
      if (balance > 0) s"Bravo $user vous êtes dans le vert"
      else s"$user vous êtes encore dans le rouge"
    }
  }

  /*
    TADA, un future se lance dès qu'il est évaluer (modulo le temps de lui trouver de la place sur
    le cpu)
    La récupération du user et de la balance se font en parallèle \o/

    Cela dit, un future a besoin d'un ExecutionContext pour travailler. Heureusement on lui en a
    fournit un. Comment ça vous l'avez pas vu ? Regardez donc ligne 3.

    Future{ expression } est du sucre syntaxique pour Future.apply(expression)
    Et la vrai signature de Future.apply est :
      Future.apply[T](body: =>T)(implicit executor: ExecutionContext): Future[T]
  */

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Exercice /////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  // Heu... La flemme !
}
