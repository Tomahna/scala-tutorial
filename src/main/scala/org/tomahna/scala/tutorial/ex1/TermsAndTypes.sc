/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Ce fichier est une WorkSheet scala, il ne s'agit pas d'un véritable fichier scala mais d'un outil
  de prototypage rapide. Chaque ligne est évalué directement à la volée

  Sources:
  https://www.scala-exercises.org/scala_tutorial/terms_and_types
 */

/*********** Expressions primitives ******************/
/*
 Le nombre 1, normalement vous devriez voir s'afficher:
  res0: Int = 1
 Il s'agit d'un nom quelconque suivi du type de la valeur et enfin son évaluation
  */
1

// Le Booléen true
true

// Le text "Hello, Scala!"
"Hello, Scala !"

/*********** Expressions Composées ******************/
/*
  De même, l'expression 1 + 2 est évaluée et donne son type
 */
1 + 2

// Le résultat de la concaténation des text "Hello, " et "Scala!"
"Hello, " ++ "Scala!"

/********************** Evaluation ******************/
/*
  Une expression non primivive est évalué comme suit
  1. On prend l'opérateur le plus à gauche
  2. On évalue ces opérandes (d'abord gauche puis droit)
  3. On applique l'opérateur aux opérandes
 */

//Voici l'évaluation d'une opération arithmétique
(1 + 2) * 3
3 * 3
9

/******************** Appel de méthode **************/
/*
  Une autre façon de rendre une expression complexe plus simple est d'appeler une méthode
 */

// Quel est la taille du texte “Hello, Scala!”
"Hello, Scala!".size

// L'objet sur lequel la méthode est appliquée est appelé l'object cible
// Les entiers de 0 à 10
1.to(10)

// Les méthodes peuvent avoir des paramètres. Ils sont fournis entre les parenthèses

/*********** Les opérateurs sont des méthodes ******/
// Les opérateurs sont simplement des méthodes avec un symbol pour nom
3 + 2 == 3.+(2)

// La syntax infix permet de ne pas mettre les parenthèses et le point
1.to(10) == (1 to 10)

// N'importe quel méthode avec un paramètre peut utiliser la syntax infix

/******************* Valeurs et types *************/
// Les expressions ont une valeur et un type
// 0 et 1 sont des nombres et ont pour type Int
0
1

// "foo" et "bar" sont des textes et leur type est String
"foo"
"bar"

/****************** Typage statique ***************/
// Le compilateur scala vérifie que vous ne combinez pas des expressions incompatibles
// Essayez de taper 1 to "foo"

/****************** Types communs  ***************/
/*
    Int: entiers encodés sur 32-bit (e.g. 1, 23, 456)
    Long: entiers encodés sur 64-bit (e.g. 1, 23, 456, 2^50)
    Double: nombres flotants encodés sur 64-bit (e.g. 1.0, 2.3, 4.56)
    Boolean: booleens (true and false)
    String: texte (e.g. "foo", "bar")
 */
