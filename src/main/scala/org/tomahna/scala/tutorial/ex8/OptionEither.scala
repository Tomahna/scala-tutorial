package org.tomahna.scala.tutorial.ex8

/*
  Ceci est un commentaire, pour écrire des commentaires trois solutions
  1) //  pour les commentaires sur une ligne
  2) /*  pour les commentaires sur plusieurs lignes */
  3) /** pour la scaladoc */

  Le symbol ??? représente un morceau de code non implémenté, le but de ces exercices est de
  correctement remplir les fragments manquant.

  Vous pouvez expérimenter avec la WorkSheet fournit dans le même package

  Vous pouvez vérifier votre solution en lançant les tests unitaires associé à l'éxercice en lançant
  la commande sbt "testOnly org.tomahna.scala.tutorial.ex8.OptionEitherSpec" dans le répertoire du projet

  Vous pouvez également lancer les tests en continue en lançant la commande
  sbt "~testOnly org.tomahna.scala.tutorial.ex8.OptionEitherSpec"

  Sources:
  https://www.scala-exercises.org/std_lib/options
 */
object OptionEither {
  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Option ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    A ce point, il y a deux sujets que j'ai soigneusement évité d'aborder. Parlons donc du premier.
    La pire horreur de l'histoire de l'informatique, le plus gros générateur de bug au monde.
    J'ai nommé le pointeur null.

    https://www.infoq.com/presentations/Null-References-The-Billion-Dollar-Mistake-Tony-Hoare

    Le concept de null existe en Scala, cela dit celui que je prend à utiliser un null en scala
    me doit un petit déjeuner.

    Bref ! Pour marquer la possibilité d'une absence de valeur, on utilise un ADT (cf ex5) qu'on
    appelle un Option. Et comme c'est un ADT on peut utiliser le pattern matching dessus !
    Il s'agit d'un type qui peut avoir 2 valeurs soit Some(valeur) soit None
   */

  val present: Option[String] = Option("test") // Valeur présente
  val absent: Option[String] = None //Valeur Absente

  /*
   Attention, parfois l'inférence de type peut jouer des tours

   Le type de inferenceFoireuse est None.type.

   Le compilateur a raison, il ne peut pas deviner le type souhaiter. Ca peut être Option[Int], Option[Cacahuete].
   La c'est assez facile, mais en pratique on peut avoir quelques surprises de temps en temps
   */
  val inferenceFoireuse = None

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Méthodes /////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Les options ont globalement les mêmes méthodes de base que les collections et elles se comporte
    de la même façon. Il s'agit en fait d'une collection d'une taille 0 ou 1.

    map -> applique une opération à l'élement si il est présent de la liste
    flatmap -> applique une opération qui renvoit une option à l'élément si il est présent
    filter -> applique un filtre à l'option
   */
  val basePresent = Some(2)
  basePresent.map(_ * 2) // _ * 2 est une façon abrégé d'écrire x => x * 2, et ça donne Some(4)
  basePresent.filter(_ == 0) // pareil, _ == 0 est équivalent à x => x == 0, et ça donne None
  //Bref allez jouer dans la Worksheet et demandez si vous ne comprenez pas

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Exercice /////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /**
    * Take the first element of the list
    * Apply the toLowerCase transformation
    * Verify if it's barry
    * If it's barry return "Hello, Barry!" else "Meh
    *
    * Don't use if/else, don't use pattern matching
    *
    * Hint: the last method to use starts with a f
    */
  def helloBarry(s: List[String]): String = ???

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Either ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    De la même façon que l'option représente une absence ou une présence d'élement. Un either représente
    l'union de deux types. La différence c'est que les deux types sont paramètrables. C'est aussi un ADT.
   */
  val left: Either[String, Int] = Left("Hello")
  val right: Either[String, Int] = Right(1)

  // Même layus sur l'inférence de type que pour l'option
  /*
    Depuis Scala 2.12 le Either est biaisé à droite, cela veut dire que les fonctions comme map
    et flatMap s'appliquent sur le type de droite.
 */

  //////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Tuples ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////
  /*
    Dans l'ex5, on a parlé de types coproduits (Je suis ça OU ça)
    Scala dispose également d'une façon d'écrire des types produit (Je suis ça ET ça) sous la forme de
    tuples.
   */
  val t: (String, Int, Boolean) = ("Barry", 29, true)

  /*
    Notez que c'est très similaire au fait d'écrire une case class
   */
  case class User(name: String, age: Int, isAdmin: Boolean)
  val t2 = User("Barry", 29, true)

  /*
    La différence c'est que avec t2 on sait quel type de donnée on manipule. Donc c'est mieux.

    TLDR: Les tuples ça existe mais les utilisez que quand c'est nécessaire
   */
}
