package org.tomahna.scala.tutorial.ex2

import org.scalatest._

class DefinitionsAndEvaluationSpec extends FlatSpec with Matchers {
  "val area" should "be 314.159" in {
    DefinitionsAndEvaluation.area shouldBe (3.14159 * 10 * 10)
  }

  "area2" should "be implemented" in {
    "DefinitionsAndEvaluation.area2(2)" should compile
  }

  "sumOfSquares" should "be implemented" in {
    "DefinitionsAndEvaluation.sumOfSquares(1, 2)" should compile
  }

  "abs(-10)" should "return 10" in {
    DefinitionsAndEvaluation.abs(-10) shouldBe 10
  }
}
