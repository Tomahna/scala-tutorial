package org.tomahna.scala.tutorial.ex3

import org.scalatest._

class LexicalScopesSpec extends FlatSpec with Matchers {
  "answer" should "be correct" in {
    (LexicalScopes.answer == LexicalScopes.result) shouldBe true
  }

  "BazY" should "be correct" in {
    (Baz.y == LexicalScopes.bazY) shouldBe true
  }
}
