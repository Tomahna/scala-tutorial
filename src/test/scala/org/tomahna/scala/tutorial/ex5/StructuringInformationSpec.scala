package org.tomahna.scala.tutorial.ex5

import org.scalatest.{FlatSpec, Matchers}

class StructuringInformationSpec extends FlatSpec with Matchers {
  "Barry" should "be admin" in {
    val profile = StructuringInformation.getProfile("Barry")
    StructuringInformation.isAdmin(profile) shouldBe true
  }

  "Paul" should "be admin" in {
    val profile = StructuringInformation.getProfile("Paul")
    StructuringInformation.isAdmin(profile) shouldBe true
  }

  "Alister" should "not be admin" in {
    val profile = StructuringInformation.getProfile("Alister")
    StructuringInformation.isAdmin(profile) shouldBe false
  }
}
