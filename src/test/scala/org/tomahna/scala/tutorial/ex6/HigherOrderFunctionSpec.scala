package org.tomahna.scala.tutorial.ex6

import org.scalatest.{FlatSpec, Matchers}

class HigherOrderFunctionSpec extends FlatSpec with Matchers {
  "greetBarry" should "greet barry" in {
    HigherOrderFunction.greetBarry("Barry") shouldBe "Hello, Barry!"
  }

  "greetBarry" should "not greet paul" in {
    HigherOrderFunction.greetBarry("Paul") shouldBe "Meh"
  }

  "greetPaul" should "greet paul" in {
    HigherOrderFunction.greetPaul("Paul") shouldBe "Hello, Paul!"
  }

  "greetPaul" should "not greet barry" in {
    HigherOrderFunction.greetPaul("Barry") shouldBe "Meh"
  }
}
