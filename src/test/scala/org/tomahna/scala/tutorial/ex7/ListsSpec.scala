package org.tomahna.scala.tutorial.ex7

import org.scalatest.{FlatSpec, Matchers}

class ListsSpec extends FlatSpec with Matchers {
  "Cell longer than 10 characters" should "be filtered" in {
    Lists.csvCellLowerCases(List("sefsef56s4ef65s4ef65se4f")) shouldBe empty
  }

  "Line" should "be splitted" in {
    Lists.csvCellLowerCases(List("aze,zer,ert")) shouldBe Seq("aze",
                                                              "zer",
                                                              "ert")
  }

  "Cell" should "be lowercased" in {
    Lists.csvCellLowerCases(List("AZE")) shouldBe Seq("aze")
  }

  "sumSize" should "be correct" in {
    Lists.sumSizes(List("abc", "def", "ghj")) shouldBe 9
  }
}
