package org.tomahna.scala.tutorial.ex4

import org.scalatest.{FlatSpec, Matchers}

class TailRecursionSpec extends FlatSpec with Matchers {
  "factorialTR(3)" should "be 6" in {
    TailRecursion.factorial(3) shouldBe 6
  }

  "factorialTR(4)" should "be 6" in {
    TailRecursion.factorial(4) shouldBe 24
  }
}
