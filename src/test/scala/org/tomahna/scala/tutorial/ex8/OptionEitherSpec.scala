package org.tomahna.scala.tutorial.ex8

import org.scalatest.{FlatSpec, Matchers}

class OptionEitherSpec extends FlatSpec with Matchers {
  "helloBarry" should "greet barry" in {
    OptionEither.helloBarry(List("bArrY", "Paul")) shouldBe "Hello, Barry!"
  }

  "helloBarry" should "not greet anyone else" in {
    OptionEither.helloBarry(List("Paul", "barry")) shouldBe "Meh"
  }
}
