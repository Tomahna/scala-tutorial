import Dependencies._

scalaVersion := "2.12.3"

organization := "fr.tomahna"
name := "scala-tutorial"
version := "0.1.0-SNAPSHOT"

libraryDependencies += scalaTest % Test
